Rails.application.routes.draw do
  get 'checkout/empty_cart'

  resources :checkout
  resources :products
end
