class ChangeProductsIdUnique < ActiveRecord::Migration[5.2]
  def change
    change_column :products, :product_id, :string, uniqueness: true
  end
end
