require "json"
require 'ruk1'

class CheckoutController < ApplicationController
  def index
    @products = Product.all.order("product_id asc")

    if cookies[:cart].blank?
      cookies[:cart] = JSON.generate([])
    end
    @carts = JSON.parse(cookies[:cart])


    promotional_rules = [1,2]
    co = Ruk1::Checkout.new(promotional_rules)
    @carts.each do |item|
      item['quantity'].times do
         co.scan(item)
      end
    end

    @total_price = co.total
    @total_quantity = co.total_quantity
  end

  def create
    if cookies[:cart].blank?
      cookies[:cart] = JSON.generate([])
    end
    @carts = JSON.parse(cookies[:cart])

    product_id = params[:product_id]
    product_name = params[:product_name]
    product_price = params[:product_price]

    found_at = -1
    @carts.each_with_index do |cart, index|
      if cart['product_id'] == product_id
        found_at = index
      end
    end

    if found_at == -1
      @carts << {
        product_id: product_id,
        product_name: product_name,
        product_price: product_price.to_f,
        quantity: 1
      }
    else
      @carts[found_at]['quantity'] += 1
    end

    cookies[:cart] = @carts.to_json

    redirect_to checkout_index_path
  end

  def empty_cart
    cookies.delete :cart
    redirect_to checkout_index_path
  end

  def remove_item_on_cart
    redirect_to checkout_index_path
  end

end
