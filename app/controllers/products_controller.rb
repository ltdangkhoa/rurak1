class ProductsController < ApplicationController
  def index
    @page_title       = 'Index'
    @page_description = 'Index page.'
    @page_keywords    = 'Index, Index, Index'
    @products = Product.all.order("updated_at desc")
  end

  def show
    @product = Product.find(params[:id])

    @page_title       = @product.product_name
    @page_description = @product.product_name + ' page.'
    @page_keywords    = @product.product_name
  end

  def new
    @products = Product.order("created_at desc").limit(3)
  end

  def create
    @product = Product.new(product_params)
    @product.save

    redirect_to new_product_path
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    redirect_to products_path
  end

  private
    def product_params
      params.require(:product).permit(:product_id, :product_name, :product_price)
    end
end
