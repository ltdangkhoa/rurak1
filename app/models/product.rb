class Product < ApplicationRecord
  validates_uniqueness_of :product_id
  validates_length_of :product_id, :within => 1..20, :too_long => "pick a shorter id", :too_short => "pick a longer id"
  validates_length_of :product_name, :within => 1..128, :too_long => "pick a shorter name", :too_short => "pick a longer name"
  validates_length_of :product_price, :within => 1..18, :too_long => "pick a valid value", :too_short => "pick a valid value"
end
